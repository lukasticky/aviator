+++
title = "Yosemite Valley"
description = "Photo by Bailey Zindel"
date = 2020-05-02T19:10:05Z
[extra]
thumbnail = "bailey-zindel-NRQV-hBF10M-unsplash.jpg"
+++

# Yosemite Valley, US

I was freezing cold staying in the tent cabins in Half Dome (formerly Curry) Village in Yosemite Valley with my dad who was there for work, so I got up and drove around the valley at 6 am. There’s been lots of fires in the area and the valley was filled with this thick smoke. Stopped at a turnout near Tunnel View and shot this photo - I really love the symmetry of it and the smooth gradients the smoke produces.