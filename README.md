# Aviator theme

![](screenshot.png)

A dead-simple theme for [Zola](https://getzola.org/) - perfect for portfolios.

**Note: This theme does _not_ support [sections](https://www.getzola.org/documentation/content/section/)**

## Usage

### Setup

Check out [Aviator template](https://gitlab.com/lukasticky/aviator-template) with GitLab pages already pre-configured.

---

If you want to start from scratch, use these commands to get started, or simply download & save this repository in your `themes` directory:

```
zola init
git init
git submodule add https://gitlab.com/lukasticky/aviator themes/aviator
```

Activate the theme by adding this to your `config.toml`:

```
theme = "aviator"
```

### Structure

Have a look at the [`content`](content/) and [`static`](static/) directories.

### Theme variables

The following variables can be modified under `[extra]` in your `config.toml`.

#### `aviator_primary`

Sets the themes primary color value. Make sure they work on black and white

* Type: `String`
* Default: `hsl(322, 100%, 82%)`

#### `aviator_logo`

Sets the logo path relative to the base url

* Type: `String`
* Default: `/logo.png`

#### Themes

Aviator comes in light and dark. By default, the color scheme will be chosen according to the users preference.

##### `aviator_dark`

Disables dark color scheme if set to false

* Type: `bool`
* Default: `true`

##### `aviator_light`

Disables light color scheme if set to false

* Type: `bool`
* Default: `true`

#### `aviator_copy_holder`

Sets the copyright holder name in the footer

* Type: `String`
* Default: ` `

Preview:

> (C)2020 by *aviator_copy_holder*

#### `aviator_copy_link`

Sets the copyright holder link in the footer. `aviator_copy_holder` has to be set for this to work

* Type: `String`
* Default: ` `

Preview:

> (C)2020 by *[aviator_copy_link](https://example.com/)*

#### `aviator_credits`

Enables credits in the footer

* Type: `bool`
* Default: `true`

Preview:

> [Aviator theme](https://gitlab.com/lukasticky/aviator) by [Lukas Kasticky](https://kasticky.cf/)

### Page variables

The following variables can be modified under `[extra]` in your `*.md` page files.

#### `thumbnail`

Sets the page thumbnail relative to the page path

* Type: `String`
* Default: `thumbnail.jpg`
